import { takeLatest } from 'redux-saga/effects';
import { LOAD_DIETS } from 'app/redux/constants';

function* fetchDiets() {
  // writing side effects
}

export default function* dietSaga() {
  yield takeLatest(LOAD_DIETS, fetchDiets);
}
