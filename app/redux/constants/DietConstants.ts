export const LOAD_DIETS = 'tomady/App/redux/constants/LOAD_DIETS';
export const LOAD_DIETS_SUCCESS =
  'tomady/App/redux/constants/LOAD_DIETS_SUCCESS';
export const LOAD_DIETS_ERROR = 'tomady/App/redux/constants/LOAD_DIETS_ERROR';
