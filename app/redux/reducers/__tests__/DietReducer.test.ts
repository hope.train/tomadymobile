import produce from 'immer';

import dietReducer from '../DietReducer';
import {
  initDiets,
  loadDiets,
  dietsLoaded,
  dietsLoadingError,
} from '../../actions';
import { ERROR } from '../../constants';

describe('DietReducer fetching', () => {
  const initialState = {
    loading: false,
    error: false,
    diets: [],
  };

  test('it should always return initialState', () => {
    const state = dietReducer(initialState, initDiets());
    expect(state).toStrictEqual(initialState);
  });

  test('it should start loading on LOAD_DIET action is fired ', () => {
    const state = dietReducer(initialState, loadDiets());
    const nextState = produce(initialState, (draft) => {
      draft.loading = true;
      draft.error = false;
    });
    expect(state).toStrictEqual(nextState);
  });

  test('it should udpate diets array on LOAD_DIET_SUCCESS action fired', () => {
    const diets = [{ label: 'diet1', category: ['fitness', 'childrend'] }];
    const state = dietReducer(initialState, dietsLoaded(diets));
    const nextState = produce(initialState, (draft: any) => {
      draft.diets = diets;
      draft.loading = false;
    });
    expect(state).toStrictEqual(nextState);
  });

  test('it should set error message on LOAD_DIET_ERROR action fired ', () => {
    const state = dietReducer(initialState, dietsLoadingError(ERROR));
    const nextState = produce(initialState, (draft: any) => {
      draft.error = ERROR;
      draft.loading = false;
    });
    expect(state).toStrictEqual(nextState);
  });
});
