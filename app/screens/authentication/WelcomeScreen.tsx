import * as React from 'react';
import { View, Text } from 'react-native';

function WelcomeScreen() {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>WelcomeScreen</Text>
    </View>
  );
}

export default WelcomeScreen;
