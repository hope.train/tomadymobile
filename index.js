/**
 * @format
 */

import React from 'react';
import { AppRegistry } from 'react-native';
import { ApolloProvider } from '@apollo/react-hooks';
import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import 'react-native-gesture-handler';

import { name as appName } from './app';
import Root from './Root';
import configureStore from './app/configureStore';
import configureApollo from './app/configureApollo';

const store = configureStore();
const client = configureApollo();

const App = () => (
  <ApolloProvider client={client}>
    <Provider store={store}>
      <NavigationContainer>
        <Root />
      </NavigationContainer>
    </Provider>
  </ApolloProvider>
);

AppRegistry.registerComponent(appName, () => App);
