/**
 * Combine all reducers in this file and export the combined reducers.
 */
import { combineReducers } from 'redux';

import appReducer from './AppReducer';
import dietReducer from './DietReducer';

/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */
export const createReducer = (injectedReducers = {}) => {
  const rootReducer = combineReducers({
    app: appReducer,
    diet: dietReducer,
    ...injectedReducers,
  });

  return rootReducer;
};
