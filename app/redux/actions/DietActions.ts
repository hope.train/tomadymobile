import {LOAD_DIETS, LOAD_DIETS_ERROR, LOAD_DIETS_SUCCESS} from '../constants';

export function initDiets() {
  return {type: '@@INIT', payload: {}};
}

/**
 * Load the repositories, this action starts the request saga
 *
 * @return {object} An action object with a type of LOAD_REPOS
 */
export function loadDiets() {
  return {
    type: LOAD_DIETS,
    payload: {},
  };
}

/**
 * Dispatched when the repositories are loaded by the request saga
 *
 * @param  {array} repos The repository data
 * @param  {string} username The current username
 *
 * @return {object}      An action object with a type of LOAD_REPOS_SUCCESS passing the repos
 */
export function dietsLoaded(diets: Array<any>) {
  return {
    type: LOAD_DIETS_SUCCESS,
    payload: {
      diets,
    },
  };
}

/**
 * Dispatched when loading the repositories fails
 *
 * @param  {object} error The error
 *
 * @return {object}       An action object with a type of LOAD_REPOS_ERROR passing the error
 */
export function dietsLoadingError(error: any) {
  return {
    type: LOAD_DIETS_ERROR,
    payload: {
      error,
    },
  };
}
