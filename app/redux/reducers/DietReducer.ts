/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can
 * update our application state. To add a new action,
 * add it to the switch statement in the reducer function
 *
 */

import produce from 'immer';

import { LOAD_DIETS, LOAD_DIETS_ERROR, LOAD_DIETS_SUCCESS } from '../constants';

// The initial state of the App
const initialState = {
  loading: false,
  error: false,
  diets: [],
};

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const dietReducer = (
  state = initialState,
  action: { type: any; payload: any },
) =>
  produce(state, (draft) => {
    switch (action.type) {
      case LOAD_DIETS:
        draft.loading = true;
        draft.error = false;
        break;

      case LOAD_DIETS_SUCCESS:
        draft.diets = action.payload.diets;
        draft.loading = false;
        break;

      case LOAD_DIETS_ERROR:
        draft.error = action.payload.error;
        draft.loading = false;
        break;
      default:
        break;
    }
  });

export default dietReducer;
