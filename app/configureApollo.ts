import ApolloClient, { PresetConfig } from 'apollo-boost';

const configureApollo = () => {
  const config: PresetConfig = {
    uri: 'https://48p1r2roz4.sse.codesandbox.io',
  };
  return new ApolloClient(config);
};

export default configureApollo;
